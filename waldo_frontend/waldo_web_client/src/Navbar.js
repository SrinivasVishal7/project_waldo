import React, { Component } from 'react'
import AppBar from '@material-ui/core/AppBar'
import ToolBar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'

class Navbar extends Component {
  render () {
    return (
      <div>
        <AppBar position='static'>
          <ToolBar>
            <Typography variant='title' color='inherit'>
                            Project Waldo
            </Typography>
          </ToolBar>
        </AppBar>
      </div>
    )
  }
}

export default Navbar
